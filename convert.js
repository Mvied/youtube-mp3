const path = require('path')
const fs = require('fs');
const ytdl = require("ytdl-core")
const YoutubeMp3Downloader = require('youtube-mp3-downloader')
let args = process.argv.slice(2)

//Configure YoutubeMp3Downloader with your settings
const YD = new YoutubeMp3Downloader({
	'ffmpegPath': process.env.NODE_ENV === 'production' ? 'ffmpeg' : path.resolve('node_modules', '@ffmpeg-installer', 'win32-x64', 'ffmpeg.exe'),
	'outputPath': path.resolve('mp3'),
	'youtubeVideoQuality': 'highest',       // What video quality should be used?
	'queueParallelism': 2,                  // How many parallel downloads/encodes should be started?
	'progressTimeout': 2000                 // How long should be the interval of the progress reports
})
let id = 0
args.forEach(function (arg) {
	if (arg.match(/--id=/)) {
		id = arg.match(/--id=(.*)/)[1]
	}
})
if (!id)
	throw new Error('Invalid ID')

let videoUrl = YD.youtubeBaseUrl + id,
	task = {
		overwrite: false,
		videoId: id,
		filename: null
	}
ytdl.getInfo(videoUrl, function (err, info) {
	if (err) {
		process.stderr.write(JSON.stringify(err));
		return;
	}
	let videoTitle = YD.cleanFileName(info.title)
	let artist = 'Unknown'
	let title = 'Unknown'

	if (videoTitle.indexOf('-') > -1) {
		let temp = videoTitle.split('-')
		if (temp.length >= 2) {
			artist = temp[0].trim()
			title = temp[1].trim()
		}
	} else {
		title = videoTitle
	}

	//Derive file name, if given, use it, if not, from video title
	let fileName = (task.fileName ? path.join(YD.outputPath, task.fileName) : path.join(YD.outputPath, videoTitle + '.mp3'));

	if (fs.existsSync(fileName) && !task.overwrite) {
		process.stdout.write(JSON.stringify({
			success: true
		}));
	} else {
		//Download video and save as MP3 file
		YD.download(id);

		YD.on("queueSize", function(size) {
			//console.log(size);
		});

		YD.on('finished', function (err, data) {
			process.stdout.write(JSON.stringify({
				success: true
			}));
		})

		YD.on('error', function (error) {
			process.stderr.write(JSON.stringify(error))
		})

		YD.on('progress', function (progress) {
			process.stdout.write(JSON.stringify(progress));
		})
	}
});