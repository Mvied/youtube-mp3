const production = (process.env.NODE_ENV || '').toLowerCase() === 'production';
const util = require('util');
const path = require('path');
const fs = require('fs');
const cp = require('child_process');
const helmet = require('helmet');
const express = require('express');
const serveStatic = require('serve-static');
const winston = require('winston');
const morgan = require('morgan');
const ytdl = require('ytdl-core')
const YoutubeMp3Downloader = require('youtube-mp3-downloader')
const YD = new YoutubeMp3Downloader();
const port = process.env.PORT || '9999';
// create express app
const app = express();
const router = express.Router();
// const socketIO = require('socket.io');
// const server = require('http').createServer(app);
const bodyParser = require('body-parser');
const RateLimit = require('express-rate-limit')
// const io = socketIO(server);
const limit = new RateLimit({
	windowMs: 15*60*1000, // 15 minutes
	max: 100, // limit each IP to 100 requests per windowMs
	delayMs: 0, // disable delaying - full speed until the max limit is reached
	headers: true
});

const logger = new winston.Logger();
logger.stream = {
	write: function(message, encoding){
		logger.info(message);
	}
};
logger.add(winston.transports.File, {
	filename: path.join(process.cwd(),'application.log'),
	handleExceptions: true,
	exitOnError: false,
	level: 'warn'
});
logger.add(winston.transports.Console, {
	colorize: true,
	timestamp: true,
	level: 'verbose'
});
const requestLogger = new winston.Logger();
requestLogger.stream = {
	write: function(message, encoding){
		requestLogger.log('info', message);
	}
};
requestLogger.add(winston.transports.File, {
	filename: path.join(process.cwd(),'request.log'),
	handleExceptions: true,
	exitOnError: false,
	level: 'info'
});
// request logging
app.use(morgan('{"remote_addr": ":remote-addr", "remote_user": ":remote-user", "date": ":date[clf]", "method": ":method", "url": ":url", "http_version": ":http-version", "status": ":status", "result_length": ":res[content-length]", "referrer": ":referrer", "user_agent": ":user-agent", "response_time": ":response-time"}', {
	stream: requestLogger.stream
}));

function formatArgs(args){
	return [util.format.apply(util.format, Array.prototype.slice.call(args))];
}
console.log = function(){
	logger.info.apply(logger, formatArgs(arguments));
};
console.info = function(){
	logger.info.apply(logger, formatArgs(arguments));
};
console.warn = function(){
	logger.warn.apply(logger, formatArgs(arguments));
};
console.error = function(){
	logger.error.apply(logger, formatArgs(arguments));
};
console.debug = function(){
	logger.debug.apply(logger, formatArgs(arguments));
};

router.use(bodyParser.json());
router.use(helmet());
router.use(function(req, res, next) {
	let origin = 'http://localhost:9998';
	if (!origin) {
		next(new Error('Origin header required.'));
	} else {
		// Disable Cache
		res.set('Cache-Control', 'no-cache');
		res.set('Pragma', 'no-cache');
		res.set('Expires', '0');

		// Cross-Origin Resource Sharing
		res.set('Access-Control-Allow-Origin', origin);
		res.set('Access-Control-Allow-Credentials', 'true');

		// Allowed Methods
		res.set('Access-Control-Allow-Methods', 'GET,POST');

		// Allowed Headers
		res.set('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Cache-Control');

		next();
	}
});
router.use('/', serveStatic(path.resolve('public')));
router.use('/mp3', serveStatic(path.resolve('mp3')));

let idRegex = /^[a-zA-Z0-9-_]{11}$/;
let completeConversions = [],
	activeConversions = {},
    conversions = {};

router.get('/status/:id', function(req, res, next) {
	let id = req.params.id;
	if (!id || !id.match(idRegex))
		return next(new Error('Invalid ID.'));
	if (activeConversions[id]) {
		res.json({
			success: true,
			status: 'In progress',
			videoId: activeConversions[id].videoId,
			title: activeConversions[id].title,
			progress: activeConversions[id].progress
		})
		return;
	}
	ytdl.getInfo('http://www.youtube.com/watch?v=' + id, function (err, info) {
		if (err) {
			next(err);
			return;
		}
		let videoTitle = YD.cleanFileName(info.title);
		let fileName = path.join('mp3', videoTitle + '.mp3');
		if (conversions[id]) {
			res.json({
				success: true,
				status: 'In progress',
				videoId: activeConversions[id] && activeConversions[id].videoId,
				progress: activeConversions[id] && activeConversions[id].progress
			});
		} else if (fs.existsSync(fileName)) {
			if (completeConversions.filter(function(c) { return c.id === id; }).length === 0)
				completeConversions.push({
					id: id,
					title: info.title
				});
			res.json({
				success: true,
				status: 'complete'
			});
		} else {
			res.json({
				success: false,
				status: 'failed'
			});
		}
	});
});

router.get('/active', function(req, res, next) {
	var array = [];
	for (var conversion in activeConversions) {
		if (activeConversions.hasOwnProperty(conversion))
			array.push(activeConversions[conversion]);
	}
	res.json(array);
})

router.get('/conversions', function(req, res, next) {
	res.json(completeConversions);
})

router.get('/download/:id', function(req, res, next) {
	let id = req.params.id;
	if (!id || !id.match(idRegex))
		return next(new Error('Invalid ID.'));
	if (activeConversions[id]) {
		res.json({
			success: true,
			message: "In progress"
		})
		return;
	}
	ytdl.getInfo('http://www.youtube.com/watch?v=' + id, function (err, info) {
		if (err) {
			next(err);
			return;
		}
		let videoTitle = YD.cleanFileName(info.title);
		let fileName = path.join('mp3', videoTitle + '.mp3');
		if (fs.existsSync(fileName)) {
			var readStream = fs.createReadStream(fileName);
			res.header('Content-Type', 'application/octet-stream');
			res.header('Content-Disposition', 'attachment;filename="' + videoTitle + '.mp3"')
			readStream.pipe(res);
		} else {
			res.json({
				success: false
			});
		}
	});
});

router.get('/convert/:id', limit, function(req, res, next) {
	let idRegex = /^[a-zA-Z0-9-_]{11}$/;
	let id = req.params.id;
	if (!id || !id.match(idRegex))
		return next(new Error('Invalid ID.'));
	if (Object.keys(conversions).length > 5) {
		res.status(503);
		return next(new Error('Server at capacity. Please try again later.'));
	}
	ytdl.getInfo('http://www.youtube.com/watch?v=' + id, function (err, info) {
		if (err) {
			next(err);
			return;
		}
		let videoTitle = YD.cleanFileName(info.title);

		if (!conversions[id] && !activeConversions[id]) {
			activeConversions[id] = {
				videoId: id,
				title: videoTitle,
				status: 'In progress',
				progress: {
					percentage: 0
				}
			}
			conversions[id] = cp.exec('node ./convert.js --id=' + id);
			setTimeout(function () {
				res.json({
					success: true
				})
			}, 3000)
		} else {
			res.json({
				success: true
			});
			return;
		}

		// pipe output to console
		conversions[id].stdout.pipe(process.stdout);

		conversions[id].stdout.on('data', function (data) {
			if (data) {
				try {
					if (typeof data === 'string')
						data = JSON.parse(data);
				} catch (err) {
					console.error(err);
					return;
				}

				if (data.progress) {
					data.title = videoTitle;
					activeConversions[id] = data;
				} else {
					/*res.json({
						success: true
					})*/
				}
			}
		});

		conversions[id].stderr.on('data', function (data) {
			/*if (data) {
				try {
					data = JSON.parse(data);
					next(new Error(data.error));
				} catch (err) {
					next(new Error('Internal Server Error'));
				}
			}*/
		});

		conversions[id].on('uncaughtException', function (err) {
			//console.log(err);
			//next(new Error('Internal Server Error'));
		});

		conversions[id].on('close', function (code) {
			if (code === 0) {
				/*res.json({
					success: true
				})*/
			} else {
				// next(new Error('Process ended with code ' + code));
			}
			delete conversions[id]
			delete activeConversions[id]
		});
	});
});


// 404
router.use(function(req, res, next) {
	// res.status(404);
	if(!res.headersSent)
		res.json({
			success: false,
			code: 404,
			error: 'Not found'
		});
});

// 500
router.use(function(error, req, res, next) {
	var result = {
		success: false,
		error: 'Internal Server Error'
	};

	if (error instanceof Error) {
		// res.status(500);
		console.error(error);
		result.error = error.message;
		if (app.get('env') === 'development')
			result.stack = error.stack;
	}
	if(!res.headersSent)
		res.json(result);
});

app.use(router);

app.listen(port);

console.log("Express application is up and running on port " + port);