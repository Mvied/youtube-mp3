// 20 years later people still don't know how to instantiate jQuery properly. Like dis:
jQuery(document).ready(function($) {
	var idRegex = /[a-zA-Z0-9-_]{11}/,
	  urlRegex = /^https?:\/\/(youtu.be\/|www.youtube.com\/watch\?v=)([a-zA-Z0-9-_]{11})/,
	  form = $('form'),
	  currentConversion,
	  conversionsCompleteArray = [],
	  conversionsContainer = $('.conversions'),
	  conversionsActive = $('.conversions-active'),
	  conversionsComplete = $('.conversions-complete'),
	  progressContainer = $('.progress-wrap'),
	  progressControl = $('.progress-wrap .progress-bar'),
	  searchContainer = $('.search-container'),
	  searchControl = $('input.search'),
	  autoCompleteDiv = $('<div class="search-autocomplete" style="display:none" />'),
	  isSearching = false,
	  _autoCompleteClick = function(e) {
		  e.preventDefault();
		  var id = $(this).data('id');
		  if (id.match(idRegex)) {
			  convertVideo(id);
		  }
	  },
	  statusDiv = $('.status'),
	  submitControl = $('button[type="submit"]');

	$(document.body).on('click', function(e) {
		if (autoCompleteDiv.css('display') !== 'none' && e.target === document || e.target === document.body) {
			e.preventDefault();
			autoCompleteDiv.css('display', 'none');
			autoCompleteDiv.css('opacity', '0');
		}
	});

	searchContainer.append(autoCompleteDiv);

	function showConversions() {
		$.ajax('conversions/', {
			dataType: 'json'
		}).then(function(conversions) {
			conversionsCompleteArray = conversions;
			var list = conversionsComplete.find('ul');
			list.empty();
			if (conversions.length === 0) {
				conversionsComplete.css('display', 'none');
				return;
			}
			conversions.forEach(function(conversion) {
				list.append('<li><a href="download/' + conversion.id + '" target="_blank">' + conversion.title + '</a></li>');
			})
			conversionsComplete.append(list);
			conversionsComplete.css('display', '');
		}, function(err) {
			console.error(err);
			clearInterval(conversionInterval);
		});

		$.ajax('active/', {
			dataType: 'json'
		}).then(function(conversions) {
			var list = conversionsActive.find('ul');
			list.empty();
			if (conversions.length === 0) {
				conversionsActive.css('display', 'none');
				return;
			}
			conversions.forEach(function(conversion) {
				list.append('<li><a href="https://youtu.be/' + conversion.videoId + '" target="_blank">' + conversion.title + '</a> ' + (conversion.progress ? conversion.progress.percentage.toFixed(0) + '%' : '') + '</li>');
			})
			conversionsActive.append(list);
			conversionsActive.css('display', '');
		}, function(err) {
			console.error(err);
			clearInterval(conversionInterval);
		});
	}
	showConversions();
	var conversionInterval = setInterval(showConversions, 2000);


	var key = 'AIzaSyA1-BNwtwAi1aEwhNq8ARSESHIUyZGrHoM';
	function getYouTubeVideoById(videoId) {
		// YouTube Data API base URL (JSON response)
		var url = "https://content.googleapis.com/youtube/v3/videos?part=snippet&type=video"

		url += "&id=" + videoId;

		// set paid-content as false to hide movie rentals
		//url += '&paidContent=false';

		url += '&key=' + key;

		return $.getJSON(url);
	}

	function searchYouTube(searchString) {
		if (isSearching)
			return;
		searchString = searchString.trim();
		var searchPromise,
		  searchUrl = searchString.match(urlRegex),
		  searchId = searchString.match(idRegex);
		if (searchString === '') {
			isSearching = false;
			return;
		} else if (searchId || searchUrl) {
			if (searchUrl)
				searchId = searchUrl[2];
			searchPromise = getYouTubeVideoById(searchId);
		} else {
			isSearching = true;

			// YouTube Data API base URL (JSON response)
			var url = "https://content.googleapis.com/youtube/v3/search?part=snippet&type=video"

			if (searchId)
				url += "&id=" + searchId;
			else
				url += "&q=" + searchString;

			// set paid-content as false to hide movie rentals
			//url += '&paidContent=false';

			url += '&key=' + key;

			// set duration as long to filter partial uploads
			url += '&videoDuration=long';

			// order search results by view count
			url += '&order=viewCount';

			// we can request a maximum of 50 search results in a batch
			url += '&maxResults=20';

			searchPromise = $.getJSON(url);
		}
		searchPromise.then(function (json) {
			if (json && json.items) {
				var items = json.items;
				var list = $('<ul/>');

				items.forEach(function (item) {
					var listItem = $('<li/>'),
					  link = $('<a href="#" data-id="' + (typeof item.id === 'string' ? item.id : item.id.videoId) + '" data-title="' + item.snippet.title + '">' +
						'<img src="https://i.ytimg.com/vi/' + (typeof item.id === 'string' ? item.id : item.id.videoId) + '/default.jpg">' +
						'<span class="title">' + item.snippet.title + '</span>' +
						' &ndash; <span class="channel">' + item.snippet.channelTitle + '</span>' +
						'</a>');
					link.on('click', _autoCompleteClick);
					list.append(listItem.append(link));
				});

				// Did YouTube return any search results?
				if (items.length === 0) {
					autoCompleteDiv.html(list.append('<li>No videos found</li>'));
				} else {
					// Display the YouTube search results
					autoCompleteDiv.html(list);
				}
				autoCompleteDiv.css('display', 'block');
				autoCompleteDiv.css('opacity', '1');
				isSearching = false;
			}
		}, function(err) {
			console.error(err);
		});
	}

	function moveProgressBar(percentage) {
		progressContainer.data('progress-percent', parseFloat(percentage));
		var getPercent = (parseFloat(percentage) / 100);
		var getProgressWrapWidth = progressContainer.width();
		var progressTotal = getPercent ? (getPercent * getProgressWrapWidth) : 0;
		var animationLength = 2500;

		progressContainer.css('display', '');
		progressControl.css('display', '');
		progressControl.css('opacity', '1');
		progressControl.stop().animate({
			left: progressTotal
		}, animationLength);
	}

	function convertVideo(id) {
		if (currentConversion) {
			alert('Conversion in progress. Please wait.');
			return;
		}
		progressContainer.css('display', '');
		statusDiv.text('Starting conversion...');

		searchControl.val('');
		autoCompleteDiv.find('a').off('click', _autoCompleteClick);
		autoCompleteDiv.empty();
		autoCompleteDiv.css('display', 'none');
		autoCompleteDiv.css('opacity', '0');
		moveProgressBar(0);
		var interval;
		function _progress(progress) {
			currentConversion = progress;
			moveProgressBar(progress.progress.percentage.toFixed(0));
			statusDiv.text('Converting ' + progress.title + ' ' + progress.progress.percentage.toFixed(0) + '%');
		}
		function _success(res) {
			moveProgressBar(100);
			progressControl.css('display', 'none');
			interval && clearInterval(interval);
			statusDiv.html('<a href="download/' + id + '/" target="_blank">' + (currentConversion && currentConversion.title || 'Download') + '</a>');
			searchControl.value = '';
			currentConversion = null;
			//window.location.href = 'download/' + id;
		}
		function _error(error) {
			currentConversion = null;
			moveProgressBar(100);
			interval && clearInterval(interval);
			console.error(error);
			statusDiv.text(error.error || 'Error: Unable to convert video :(');
		}

		var url = id.match(urlRegex);
		if (url)
			id = id[2]
		else
			id = id.match(idRegex);
		if (id) {
			$.ajax('convert/' + id, {
				dataType: 'json'
			}).then(function(res) {
				if (res.success) {
					interval = setInterval(function () {
						$.ajax('status/' + id, {
							dataType: 'json'
						}).then(function (res) {
							if (res.success && res.status && res.status.toLowerCase() === 'complete') {
								_success(res);
							} else if (res.error || res.status.toLowerCase() === 'failed') {
								_error(res);
							} else if (res.progress) {
								_progress(res);
							}
						}, _error);
					}, 2000);
				} else if (res.error) {
					_error(res);
				}
			}, _error);
		}
	}

	// on browser resize...
	$(window).resize(function() {
		moveProgressBar();
	});

	searchControl.on('focus', function(e) {
		searchYouTube($('input.search').val());
	});
	searchControl.on('paste', function(e) {
		searchYouTube($('input.search').val());
	});
	searchControl.on('keyup', _.debounce(function(e) {
		searchYouTube($('input.search').val());
	}, 200));
	searchControl.on('keyup', function(e) {
		if ($('input.search').val().trim() !== '') {
			autoCompleteDiv.find('a').off('click', _autoCompleteClick);
			autoCompleteDiv.html('<ul><li>Searching...</li></ul>');
			autoCompleteDiv.css('display', '');
			autoCompleteDiv.css('opacity', '1');
		} else {
			autoCompleteDiv.css('display', 'none');
			autoCompleteDiv.css('opacity', '0');
		}
	});

	form.on('submit', function(e) {
		e.preventDefault();
		var values = form.serialize();
		if (values.search && values.search.trim() !== '')
			convertVideo(values.search);
	});
});